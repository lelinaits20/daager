package com.lelin.daggerwithmvp.network.api

import com.lelin.daggerwithmvp.network.models.User
import io.reactivex.Flowable

class Repository(val mainApi: MainApi) {

    fun getUser():Flowable<List<User>>{
        return mainApi.getAllPost()
    }
}