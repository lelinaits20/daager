package com.lelin.daggerwithmvp.network.api

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideHttpCache(application: Application):Cache{
        val cacheSize=10*1024*1024

        return Cache(application.cacheDir,cacheSize.toLong())
    }


    @Provides
    @Singleton
    fun provideGson():Gson{
        val gson=GsonBuilder()
            gson.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            return gson.create()
    }


    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache):OkHttpClient{
        val client=OkHttpClient.Builder()
        client.cache(cache)
        return client.build()

    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson,okHttpClient: OkHttpClient):Retrofit{
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideMainApi(retrofit: Retrofit):MainApi{
        return retrofit.create(MainApi::class.java)
    }


    @Provides
    @Singleton
    fun provideRepository(mainApi: MainApi):Repository{
        return Repository(mainApi)
    }
}