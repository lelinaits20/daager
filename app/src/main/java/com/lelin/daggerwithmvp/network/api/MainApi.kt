package com.lelin.daggerwithmvp.network.api

import com.lelin.daggerwithmvp.network.models.User
import io.reactivex.Flowable
import retrofit2.Call
import retrofit2.http.GET

interface MainApi {

    @GET("posts")
    fun getAllPost():Flowable<List<User>>
    @GET("posts")
    fun getLIst():Call<List<User>>


}