package com.lelin.daggerwithmvp.dagger

import com.lelin.daggerwithmvp.network.api.ApiModule
import com.lelin.daggerwithmvp.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,ApiModule::class])
interface ApiComponent {

    fun inject(mainActivity: MainActivity)
}