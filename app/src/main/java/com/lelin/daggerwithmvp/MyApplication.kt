package com.lelin.daggerwithmvp

import android.app.Application
import com.lelin.daggerwithmvp.dagger.ApiComponent
import com.lelin.daggerwithmvp.dagger.AppModule
import com.lelin.daggerwithmvp.dagger.DaggerApiComponent
import com.lelin.daggerwithmvp.network.api.ApiModule

class MyApplication :Application(){
    private lateinit var apiComponent: ApiComponent
    override fun onCreate() {
        super.onCreate()

        apiComponent=DaggerApiComponent.builder().apiModule(ApiModule()).appModule(AppModule(this)).build()
    }

    fun getNetComponet():ApiComponent{
        return apiComponent
    }
}