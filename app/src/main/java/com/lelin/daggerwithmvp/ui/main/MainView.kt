package com.lelin.daggerwithmvp.ui.main

import com.lelin.daggerwithmvp.network.models.User

interface MainView {
    fun loadUserList(userList:List<User>)
}