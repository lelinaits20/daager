package com.lelin.daggerwithmvp.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.lelin.daggerwithmvp.MyApplication
import com.lelin.daggerwithmvp.R
import com.lelin.daggerwithmvp.network.api.Repository
import com.lelin.daggerwithmvp.network.models.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainActivity : AppCompatActivity() ,MainView{

    @Inject lateinit var repository: Repository
    lateinit var mPresenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as MyApplication).getNetComponet().inject(this)
        mPresenter= Presenter(repository,this)
        mPresenter.loadUser()

        println()

    }

    override fun loadUserList(userList: List<User>) {
        for (i in userList){
            Log.e("post","${i.id}")
        }
    }


}
