package com.lelin.daggerwithmvp.ui.main

import android.util.Log
import com.lelin.daggerwithmvp.network.api.Repository
import com.lelin.daggerwithmvp.network.models.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Presenter(val repository: Repository,val view: MainView) {

    fun loadUser(){
        repository.getUser().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onsuccess,this::onError)
    }

    fun onsuccess(list:List<User>){
        Log.e("tag",""+list.size)
        view.loadUserList(list)

    }

    fun onError(throwable: Throwable){
        Log.e("tag",throwable.localizedMessage)

    }
}